import matplotlib.pyplot as plt
import pandas
import sys
from ggplot import *
from datetime import datetime

def convertDatetime(date):
    date_object = datetime.strptime(date,"%Y-%m-%d %H:%M:%S")
    return date_object.strftime("%Y %m")

def print_details(dataframe):
    dataframe = dataframe[["month_year","PostId"]]
    grouped = dataframe.groupby("month_year").count()
    print grouped["PostId"].sum()
    grouped.to_csv("output_all_closed.csv")
    

dataframe =pandas.read_csv(sys.argv[1],dtype=object)
#dataframe = dataframe[["Id","CreationDate"]]
dataframe["month_year"]=""
dataframe["month_year"] = dataframe["CreationDate"].apply(convertDatetime)
#duplicate= dataframe[(dataframe["Comment"] == "10") | (dataframe["Comment"]=="104")]
print_details(dataframe)

#grouped = dataframe.groupby("month_year").count()
#print grouped["Id"].sum()
#grouped.to_csv("output.csv")

#plt.hist(dataframe[sys.argv[2]],bins=50,normed=True)
#plt.title("Histogram")
#plt.xlabel("Age")
#plt.ylabel("Probability_distribution")
#plt.show() 
